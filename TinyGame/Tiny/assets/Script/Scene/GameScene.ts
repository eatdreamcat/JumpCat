
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    private rootNode: cc.Node;
    
    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
    }

    start() {

    }

    update(dt) { }
}
